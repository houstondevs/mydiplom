import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 14.0),
                child: Text("Новости", style: GoogleFonts.oswald(fontSize: 24),),
              ),
              Wrap(
                spacing: 15,
                runSpacing: 15,
                children: <Widget>[
                  UsefulCard(
                    "https://sourcery.ai/static/8bd4114da4db25ae197f53bad36c7c36/12609/code.jpg",
                    "Новый раздел",
                    "Был добавлен раздел с введением в язык Python",
                  ),
                  UsefulCard(
                    "https://images.wallpaperscraft.ru/image/kod_tekst_raznotsvetnyj_140555_2048x1152.jpg",
                    "Установка",
                    "В данном разделе описана инструкция по установке, настройке Python и среды разработки",
                  ),
                  UsefulCard(
                    "https://get.pxhere.com/photo/computer-screen-light-technology-number-line-color-gadget-website-monitor-display-it-shape-code-screenshot-css-atmosphere-of-earth-computer-wallpaper-display-device-1215760.jpg",
                    "Цикл for",
                    "В данном руководстве вы ознакомитесь с работой оператора for в языке python, также мы проведем сравнительный анализ(синтаксический, скорость работы) с аналогичным оператором в языке С++",
                  ),
                  UsefulCard(
                    "https://lifeboat.com/blog.images/ai-learns-to-write-its-own-code.jpg",
                    "Ввод вывод",
                    "Ознакомление с операциями ввода вывода, работа со строками",
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class UsefulCard extends StatelessWidget {
  String imageURL;
  String title;
  String subtitle;

  UsefulCard(this.imageURL, this.title, this.subtitle);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.deepPurple[500],
          borderRadius: BorderRadius.circular(20)),
      height: 150,
      child: Row(
        children: <Widget>[
          Container(
            height: 150,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  this.imageURL,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width * 0.6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  this.title,
                  style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.amber[500],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(
                    this.subtitle,
                    style: GoogleFonts.roboto(
                      fontSize: 10,
                      color: Colors.lightBlue[200],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
