import 'package:diplom/functions/ApiRequests/requests.dart';
import 'package:diplom/settings.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  PageController pageController;
  LoginScreen({this.pageController});

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            Colors.deepPurple[800],
            Colors.deepPurple[700],
            Colors.deepPurple[600],
            Colors.deepPurple[400],
          ],
        ),
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: FractionallySizedBox(
            heightFactor: 1,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 90),
                        child: Text(
                          "Авторизация",
                          style: GoogleFonts.oswald(
                            textStyle: TextStyle(fontSize: 32),
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 80, left: 25, right: 25),
                        child: Theme(
                          data: Theme.of(context).copyWith(
                            primaryColor: Colors.tealAccent[200],
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.only(bottom: 15),
                                child: TextField(
                                  controller: _userNameController,
                                  style: TextStyle(
                                    color: Colors.tealAccent[200],
                                  ),
                                  cursorColor: Colors.tealAccent[200],
                                  decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 2.0,
                                        color: Colors.tealAccent[200],
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 2,
                                        color: Colors.red,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    prefixIcon: Icon(
                                      Icons.email,
                                    ),
                                    hintText: "Номер телефона",
                                  ),
                                ),
                              ),
                              TextField(
                                controller: _passwordController,
                                style: TextStyle(
                                  color: Colors.tealAccent[200],
                                ),
                                cursorColor: Colors.tealAccent[200],
                                obscureText: true,
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    ),
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 2.0,
                                      color: Colors.tealAccent[200],
                                    ),
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                    ),
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: Colors.red,
                                    ),
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  prefixIcon: Icon(Icons.lock),
                                  hintText: "Пароль",
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 12.0),
                                child: Text(
                                  "Забыли пароль?",
                                  style: GoogleFonts.play(color: Colors.white),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: ButtonTheme(
                                  minWidth: 100,
                                  child: RaisedButton(
                                    color: Colors.amber[700],
                                    child: Text(
                                      "Войти",
                                      style: GoogleFonts.play(
                                        color: Colors.white,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    onPressed: () {
                                      LoginRegistrationRepository().loginRequest(
                                          context,
                                          _userNameController.text,
                                          _passwordController.text);
                                      // return navigatorKey.currentState.pushReplacementNamed("/BottomBarElement");
                                    },
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 1),
                                child: FlatButton(
                                  child: Text(
                                    "Создать аккаунт",
                                    style:
                                        GoogleFonts.play(color: Colors.white),
                                  ),
                                  onPressed: () {
                                    widget.pageController.nextPage(
                                      duration: Duration(milliseconds: 850),
                                      curve: Curves.fastOutSlowIn,
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
