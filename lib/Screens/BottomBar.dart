import 'package:diplom/Screens/CourseScreen.dart';
import 'package:diplom/Screens/CoursesScreen.dart';
import 'package:diplom/Screens/MainScreen.dart';
import 'package:diplom/Screens/ProfileScreen.dart';
import 'package:diplom/settings.dart';
import 'package:flutter/material.dart';

class BottomBarElement extends StatefulWidget {
  BottomBarElement({Key key}) : super(key: key);
  @override
  BottomBarState createState() => BottomBarState();
}

class BottomBarState extends State<BottomBarElement> {
  int _selectedIndex = 0;
  final List<Widget> _tabs = [
    MainScreen(),
    CoursesScreen(),
    ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabs[_selectedIndex],
      backgroundColor: CourseColors.backgroundcolor,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Главная"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            title: Text("Курсы"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box),
            title: Text("Профиль"),
          ),
        ],
        backgroundColor: Colors.deepPurple[500],
        selectedItemColor: Colors.amber[800],
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
