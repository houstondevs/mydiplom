import 'dart:ui';
import 'package:diplom/Models/ProfileModel.dart';
import 'package:diplom/functions/Other/logout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:diplom/functions/ApiRequests/requests.dart';

class ProfileScreen extends StatefulWidget {
  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  int currentTabIndex = 0;

  List<Widget> _widgets = [
    BottomProfileInfo(),
    BottomProfileEdit(),
  ];

  changeTab(int index) {
    this.setState(() {
      this.currentTabIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          FutureBuilder(
              future: RequestsWithToken().getProfileData(),
              builder:
                  (BuildContext context, AsyncSnapshot<ProfileModel> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return new Text('No preferences');
                  case ConnectionState.waiting:
                    return Stack(
                      children: <Widget>[
                        Container(
                          child: BackdropFilter(
                            child: new Container(
                              decoration: new BoxDecoration(
                                color: Colors.white.withOpacity(0.05),
                              ),
                            ),
                            filter: ImageFilter.blur(sigmaX: 8.0, sigmaY: 3.0),
                          ),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image:
                                  AssetImage('lib/assets/images/default.png'),
                            ),
                          ),
                          height: 270,
                        ),
                        Container(
                          height: 270,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                          ),
                        ),
                        Container(
                          height: 250,
                          child: Column(
                            children: <Widget>[
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 40.0),
                                  child: CircleAvatar(
                                    radius: 60,
                                    backgroundImage: AssetImage(
                                      'lib/assets/images/default.png',
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Имя отчество",
                                      style: GoogleFonts.oswald(
                                        color: Colors.white,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 270,
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: ProfileTabItem(
                                  icon: Icons.info,
                                  onPress: () => this.changeTab(0),
                                  isActive: this.currentTabIndex == 0,
                                ),
                              ),
                              Expanded(
                                child: ProfileTabItem(
                                  icon: Icons.menu,
                                  onPress: () => this.changeTab(1),
                                  isActive: this.currentTabIndex == 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  default:
                    if (snapshot.hasError) {
                      return InkWell(
                          child: Padding(
                            padding: const EdgeInsets.all(32.0),
                            child: Text("ERROR OCCURRED, Tap to retry !"),
                          ),
                          onTap: () => setState(() {}));
                    } else {
                      return Stack(
                        children: <Widget>[
                          Container(
                            child: BackdropFilter(
                              child: new Container(
                                decoration: new BoxDecoration(
                                  color: Colors.white.withOpacity(0.05),
                                ),
                              ),
                              filter:
                                  ImageFilter.blur(sigmaX: 8.0, sigmaY: 3.0),
                            ),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                  snapshot.data.photoUrl,
                                ),
                              ),
                            ),
                            height: 270,
                          ),
                          Container(
                            height: 270,
                            decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.5),
                            ),
                          ),
                          Container(
                            height: 250,
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 40.0),
                                    child: CircleAvatar(
                                      radius: 60,
                                      backgroundImage: NetworkImage(
                                        snapshot.data.photoUrl
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        snapshot.data.firstName +
                                            " " +
                                            snapshot.data.lastName,
                                        style: GoogleFonts.oswald(
                                          color: Colors.white,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 270,
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: ProfileTabItem(
                                    icon: Icons.info,
                                    onPress: () => this.changeTab(0),
                                    isActive: this.currentTabIndex == 0,
                                  ),
                                ),
                                Expanded(
                                  child: ProfileTabItem(
                                    icon: Icons.menu,
                                    onPress: () => this.changeTab(1),
                                    isActive: this.currentTabIndex == 1,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    }
                }
              }),
          Container(
            child: _widgets[currentTabIndex],
          ),
        ],
      ),
    );
  }
}

class ProfileTabItem extends StatelessWidget {
  IconData icon;
  Function onPress;
  bool isActive;

  ProfileTabItem({this.icon, this.onPress, this.isActive});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onPress,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                color: this.isActive ? Colors.white : Colors.transparent,
                width: 2,
                style: BorderStyle.solid),
          ),
        ),
        height: 60,
        alignment: Alignment.center,
        child: Icon(
          this.icon,
          color: this.isActive ? Colors.white : Colors.white.withOpacity(0.7),
        ),
      ),
    );
  }
}

class TextIcon extends StatelessWidget {
  IconData aIcon;
  String aText;

  TextIcon({this.aIcon, this.aText});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            aIcon,
            color: Colors.deepPurple[500],
          ),
          Padding(
            padding: EdgeInsets.only(left: 15),
          ),
          Expanded(
              child: Text(
            aText,
            style: GoogleFonts.oswald(fontSize: 18),
          ))
        ],
      ),
    );
  }
}

class MenuItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 45,
          child: FlatButton(
            onPressed: () => null,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.people,
                  color: Colors.deepPurple[500],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                ),
                Text(
                  "Друзья",
                  style: GoogleFonts.oswald(
                    fontSize: 16,
                    color: Colors.amber[800],
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 45,
          child: FlatButton(
            onPressed: () => null,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.message,
                  color: Colors.deepPurple[500],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                ),
                Text(
                  "Сообщения",
                  style: GoogleFonts.oswald(
                    fontSize: 16,
                    color: Colors.amber[800],
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 45,
          child: FlatButton(
            onPressed: () => logout(),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.exit_to_app,
                  color: Colors.deepPurple[500],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                ),
                Text(
                  "Выйти",
                  style: GoogleFonts.oswald(
                    fontSize: 16,
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class BottomProfileInfo extends StatefulWidget {
  @override
  BottomProfileInfoState createState() => BottomProfileInfoState();
}

class BottomProfileInfoState extends State<BottomProfileInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      child: FutureBuilder(
          future: RequestsWithToken().getProfileData(),
          builder:
              (BuildContext context, AsyncSnapshot<ProfileModel> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return new Text('No preferences');
              case ConnectionState.waiting:
                return CircularProgressIndicator();
              default:
                if (snapshot.hasError) {
                  return InkWell(
                      child: Padding(
                        padding: const EdgeInsets.all(32.0),
                        child: Text("ERROR OCCURRED, Tap to retry !"),
                      ),
                      onTap: () => setState(() {}));
                } else {
                  return Column(
                    children: <Widget>[
                      TextIcon(aIcon: Icons.email, aText: snapshot.data.email),
                      TextIcon(
                        aIcon: Icons.location_city,
                        aText: "Город",
                      ),
                      TextIcon(
                        aIcon: Icons.school,
                        aText: "Университет",
                      ),
                    ],
                  );
                }
            }
          }),
    );
  }
}

class BottomProfileEdit extends StatefulWidget {
  @override
  BottomProfileEditState createState() => BottomProfileEditState();
}

class BottomProfileEditState extends State<BottomProfileEdit> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      child: MenuItems(),
    );
  }
}
