import 'package:diplom/Models/CardModel.dart';
import 'package:diplom/Screens/CourseScreen.dart';
import 'package:diplom/functions/ApiRequests/inerceptors.dart';
import 'package:diplom/functions/ApiRequests/requests.dart';
import 'package:diplom/settings.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CoursesScreen extends StatefulWidget {
  @override
  CoursesScreenState createState() => CoursesScreenState();
}

class CoursesScreenState extends State<CoursesScreen> {
  List<MyCard> listCards = [
    MyCard(
      img:
          "https://sourcery.ai/static/8bd4114da4db25ae197f53bad36c7c36/12609/code.jpg",
      title: "Введение",
      text:
          "Вступительная часть курса, история языка и способы его применения. Основные понятия.",
    ),
    MyCard(
      img:
          "https://images.wallpaperscraft.ru/image/kod_tekst_raznotsvetnyj_140555_2048x1152.jpg",
      title: "Установка",
      text:
          "В данном разделе описана инструкция по установке, настройке Python и среды разработки",
    ),
    MyCard(
      img:
          "https://get.pxhere.com/photo/computer-screen-light-technology-number-line-color-gadget-website-monitor-display-it-shape-code-screenshot-css-atmosphere-of-earth-computer-wallpaper-display-device-1215760.jpg",
      title: "Цикл for",
      text:
          "В данном руководстве вы ознакомитесь с работой оператора for в языке python, также мы проведем сравнительный анализ(синтаксический, скорость работы) с аналогичным оператором в языке С++",
    ),
    MyCard(
      img:
          "https://lifeboat.com/blog.images/ai-learns-to-write-its-own-code.jpg",
      title: "Ввод вывод",
      text: "Ознакомление с операциями ввода вывода, работа со строками",
    ),
  ];

  List<String> _items = ["One", "Two"];

  ListView _coursesListView(data) {
    return ListView.builder(
        padding: EdgeInsets.all(8),
        scrollDirection: Axis.horizontal,
        itemCount: data.length,
        itemBuilder: (context, index) {
          return MyCard(
            img: data[index].titleUrl,
            title: data[index].title,
            text: data[index].description,
            pk: data[index].pk,
          );
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 14, top: 10, bottom: 10),
              alignment: Alignment.centerLeft,
              child: Text(
                "Разделы",
                style: GoogleFonts.oswald(fontSize: 24),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 1.0,
                          style: BorderStyle.solid,
                          color: Colors.deepPurple[500]),
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Text(
                        "Выбор раздела",
                        style: GoogleFonts.roboto(
                          color: Colors.deepPurple[500],
                          fontSize: 12,
                        ),
                      ),
                      iconEnabledColor: Colors.deepPurple[500],
                      items: _items.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 1.0,
                          style: BorderStyle.solid,
                          color: Colors.deepPurple[500]),
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      iconDisabledColor: Colors.deepPurple[500],
                      hint: Text(
                        "Выбор темы",
                        style: GoogleFonts.roboto(
                          color: Colors.deepPurple[500],
                          fontSize: 12,
                        ),
                      ),
                      iconEnabledColor: Colors.deepPurple[500],
                      items: _items.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: Container(
                  height: 450,
                  child: FutureBuilder<List<CardModel>>(
                    future: RequestsWithToken().getCoursesList(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<CardModel> data = snapshot.data;
                        return _coursesListView(data);
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyCard extends StatelessWidget {
  String img;
  String title;
  String text;
  int pk;

  MyCard({this.img, this.title, this.text, this.pk});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(22.0),
          bottom: Radius.circular(22.0),
        ),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 5.0),
        ],
      ),
      height: 500,
      width: 300,
      child: Card(
        semanticContainer: true,
        color: Colors.deepPurple[500],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(22.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: double.maxFinite,
              height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(22.0)),
                image: DecorationImage(
                  image: NetworkImage(
                    img,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                alignment: Alignment.bottomLeft,
                child: Text(
                  title,
                  style: TextStyle(fontSize: 36, color: Colors.white),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              width: 300,
              height: 170,
              child: Text(
                text,
                overflow: TextOverflow.fade,
              ),
            ),
            Center(
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.amber[800])),
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CourseScreen(id: pk, title: title,))),
                color: Colors.amber[800],
                child: Container(
                  width: 120,
                  alignment: Alignment.center,
                  child: Text(
                    "Читать",
                    style: GoogleFonts.oswald(color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
