import 'package:diplom/Screens/RegistrationScreen.dart';
import 'package:flutter/material.dart';

import 'LoginScreen.dart';

class LogReg extends StatefulWidget {
  @override
  LogRegState createState() => LogRegState();
}

class LogRegState extends State<LogReg> {
  final pageController = PageController(
    initialPage: 0,
  );

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pageController,
      children: <Widget>[
        LoginScreen(pageController: pageController),
        RegistrationScreen(pageController: pageController),
      ],
    );
  }
}
