import 'package:flutter/material.dart';
import 'package:diplom/settings.dart';
import 'package:flare_flutter/flare_actor.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4)).then((_) {
      navigatorKey.currentState.pushReplacementNamed('/LogRegScreen');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CourseColors.splashBackground,
      body: SafeArea(
        child: Center(
          child: Container(
            padding: EdgeInsets.only(bottom: 30),
            child: FlareActor(
              "lib/assets/images/text_splash.flr",
              animation: "fill",
            ),
          ),
        ),
      ),
    );
  }
}
