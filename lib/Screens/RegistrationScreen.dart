import 'package:diplom/functions/ApiRequests/requests.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RegistrationScreen extends StatefulWidget {
  PageController pageController;
  RegistrationScreen({this.pageController});

  @override
  RegistrationScreenState createState() => RegistrationScreenState();
}

class RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  String _phoneNumber;
  String _password1;
  String _password2;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            Colors.deepPurple[800],
            Colors.deepPurple[700],
            Colors.deepPurple[600],
            Colors.deepPurple[400],
          ],
        ),
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: FractionallySizedBox(
            heightFactor: 1,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 90),
                        child: Text(
                          "Регистрация",
                          style: GoogleFonts.oswald(
                            textStyle: TextStyle(fontSize: 32),
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 80, left: 25, right: 25),
                        child: Theme(
                          data: Theme.of(context).copyWith(
                            primaryColor: Colors.tealAccent[200],
                          ),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.only(bottom: 15),
                                  child: TextFormField(
                                    style: TextStyle(
                                      color: Colors.tealAccent[200],
                                    ),
                                    cursorColor: Colors.tealAccent[200],
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          width: 2.0,
                                          color: Colors.tealAccent[200],
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.red,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.red,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      prefixIcon: Icon(
                                        Icons.email,
                                      ),
                                      hintText: "Номер телефона",
                                    ),
                                    validator: (value) {
                                      Pattern pattern = r"^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$";
                                      RegExp regex = RegExp(pattern);
                                      if (value.isEmpty || !regex.hasMatch(value)){
                                        return "Номер телефона должен быть 89999999999";
                                      } else {
                                        setState(() {
                                          _phoneNumber = value;
                                        });
                                      }
                                    },
                                  ),
                                ),
                                TextFormField(
                                  style: TextStyle(
                                    color: Colors.tealAccent[200],
                                  ),
                                  cursorColor: Colors.tealAccent[200],
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 2.0,
                                        color: Colors.tealAccent[200],
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 2,
                                        color: Colors.red,
                                      ),
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    prefixIcon: Icon(Icons.lock),
                                    hintText: "Пароль",
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Введите пароль";
                                    } else {
                                      setState(() {
                                        _password1 = value;
                                      });
                                    }
                                  },
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: TextFormField(
                                    style: TextStyle(
                                      color: Colors.tealAccent[200],
                                    ),
                                    cursorColor: Colors.tealAccent[200],
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          width: 2.0,
                                          color: Colors.tealAccent[200],
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.red,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.red,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ),
                                      prefixIcon: Icon(Icons.lock),
                                      hintText: "Пароль",
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Введите пароль";
                                      } else {
                                        setState(() {
                                          _password2 = value;
                                        });
                                      }
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ButtonTheme(
                                    minWidth: 100,
                                    child: RaisedButton(
                                      color: Colors.amber[700],
                                      child: Text("Создать",
                                          style: GoogleFonts.play(
                                            color: Colors.white,
                                          )),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                      onPressed: () {
                                        if (_formKey.currentState.validate() && _password1==_password2) {
                                          LoginRegistrationRepository().registraionRequest(context, _phoneNumber, _password2, widget.pageController);
                                        }
                                        else{
                                          print('not valid');
                                        }
                                      },
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 1),
                                  child: FlatButton(
                                    child: Text(
                                      "Уже есть аккаунт?",
                                      style:
                                          GoogleFonts.play(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      widget.pageController.previousPage(
                                        duration: Duration(milliseconds: 850),
                                        curve: Curves.fastOutSlowIn,
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
