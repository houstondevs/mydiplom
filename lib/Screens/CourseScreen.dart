import 'package:diplom/functions/ApiRequests/requests.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class CourseScreen extends StatefulWidget {
  int id;
  String title;
  CourseScreen({this.id, this.title});

  @override
  _CourseScreenState createState() => _CourseScreenState();
}

markImage(uri, context) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.9,
    height: 150,
    child: Image.network(
      uri.toString(),
      fit: BoxFit.fill,
    ),
  );
}

class _CourseScreenState extends State<CourseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.deepPurple[500],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Center(
          child: Container(
              padding: EdgeInsets.all(10),
              child: FutureBuilder(
                future: RequestsWithToken().getCourseDetail(widget.id),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: <Widget>[
                        Text(widget.title, style: TextStyle(fontSize: 24),),
                        MarkdownBody(
                          data: snapshot.data['text'],
                          styleSheet: MarkdownStyleSheet(
                            textAlign: WrapAlignment.spaceEvenly,
                            h1Align: WrapAlignment.center, 
                          ),
                          imageBuilder: (l) => markImage(l, context),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                },
              )),
        )),
      ),
    );
  }
}
