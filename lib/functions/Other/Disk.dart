import 'package:diplom/Models/LoginModel.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

saveLoginDate(Map responseJson) async{
  final storage = FlutterSecureStorage();

  if ((responseJson != null && !responseJson.isEmpty)) {
    var token = LoginModel.fromJson(responseJson).token;
    var phoneNumber = LoginModel.fromJson(responseJson).phoneNumber;
    var pk = LoginModel.fromJson(responseJson).userId;


    await storage.write(key: 'token', value: token);
    await storage.write(key: 'phoneNumber', value: phoneNumber);
    await storage.write(key: 'pk', value: pk.toString());
  } else {
    print("error write to disk");
  }
}