import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../settings.dart';

void logout(){
  final storage = new FlutterSecureStorage();
  storage.deleteAll();
  navigatorKey.currentState.pushNamedAndRemoveUntil('/LoginScreen', (Route<dynamic> route) => false);
}
