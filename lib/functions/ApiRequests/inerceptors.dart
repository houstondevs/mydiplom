import 'package:diplom/functions/Other/logout.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http_interceptor/interceptor_contract.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';



class RequestWithTokenIntereptor implements InterceptorContract{
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    final storage = new FlutterSecureStorage();
    try {
      var token = await storage.read(key: 'token');
      data.headers["Content-Type"] = "application/json";
      data.headers["Authorization"] = "Bearer $token";
    } catch (e) {
      print(e);
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
      if(data.statusCode == 200){
        return data;
      } else if(data.statusCode == 403 || data.statusCode == 401){
        logout();
      }
  }

}