import 'dart:convert';
import 'package:diplom/Models/CardModel.dart';
import 'package:diplom/Models/ProfileModel.dart';
import 'package:diplom/functions/ApiRequests/inerceptors.dart';
import 'package:diplom/functions/Other/Disk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http_client_with_interceptor.dart';
import 'package:diplom/settings.dart';

class LoginRegistrationRepository {
  Client client = HttpClientWithInterceptor.build(interceptors: [
    //
  ]);

  Future registraionRequest(
      BuildContext context, String phoneNumber, String password,
      [PageController pageController]) async {
    final url = API_URL + "/registration/";

    void _showDialog() {
      // flutter defined function
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Успешно"),
            content: new Text("Регистрация прошла успешно!"),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Ок"),
                onPressed: () {
                  Navigator.of(context).pop();
                  pageController.previousPage(
                    duration: Duration(milliseconds: 850),
                    curve: Curves.fastOutSlowIn,
                  );
                },
              ),
            ],
          );
        },
      );
    }

    Map<String, String> body = {
      "phone_number": phoneNumber,
      "password": password
    };

    final response = await client.post(
      url,
      body: body,
    );

    if (response.statusCode == 201) {
      _showDialog();
    } else {
      print("error reg");
      return null;
    }
  }

  Future loginRequest(
      BuildContext context, String phoneNumber, String password) async {
    final url = API_URL + "/token/";

    Map<String, String> body = {
      'phone_number': phoneNumber,
      'password': password,
    };

    final response = await client.post(
      url,
      body: body,
    );

    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);
      saveLoginDate(responseJson);
      return navigatorKey.currentState
          .pushReplacementNamed("/BottomBarElement");
    } else {
      print("login error");
      return null;
    }
  }
}

class RequestsWithToken {
  Client client = HttpClientWithInterceptor.build(interceptors: [
    RequestWithTokenIntereptor(),
  ]);

  Future<ProfileModel> getProfileData() async {
    ProfileModel user;
    try{
      final response = await client.get(API_URL+"/me/");
      Map responseBody = json.decode(utf8.decode(response.bodyBytes));
      user = ProfileModel.fromJson(responseBody);
    }
    catch(e){
      print(e);
      print("getProfileData error");
    }
    return user;
  }

  Future<List<CardModel>> getCoursesList() async{
    try{
      final response = await client.get(API_URL+"/courses/");
      List jsonResponse = json.decode(utf8.decode(response.bodyBytes));
      return jsonResponse.map((job) => new CardModel.fromJson(job)).toList();
    }
    catch(e){
      print(e);
      print("getCoursesList error");
    }
  }

  Future getCourseDetail(pk) async{
    try{
      print(pk);
      final response = await client.get(API_URL+"/courses/"+pk.toString()+"/");
      return json.decode(utf8.decode(response.bodyBytes));
    }
    catch(e){
      print(e);
      print("getCoursesDetail error");
    }
  }
}
