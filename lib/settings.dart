import 'package:diplom/Screens/BottomBar.dart';
import 'package:diplom/Screens/CourseScreen.dart';
import 'package:diplom/Screens/MainScreen.dart';
import 'package:diplom/Screens/RegLog.dart';
import 'package:flutter/material.dart';
import 'package:diplom/Screens/LoginScreen.dart';
import 'package:diplom/Screens/SplashScreen.dart';

import 'Screens/RegistrationScreen.dart';

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
const API_URL = "http://192.168.0.13:8000/api";

ThemeData courseTheme = ThemeData(
  scaffoldBackgroundColor: CourseColors.backgroundcolor,
);

class CourseColors {
  static Color backgroundcolor = const Color.fromRGBO(250, 250, 250, 1);
  static Color splashBackground = const Color.fromRGBO(17, 17, 17, 1);
  static Color loginBackground = const Color.fromRGBO(62, 186, 247, 1);
}


Map<String, WidgetBuilder> courseRoutes = {
  '/SplashScreen': (BuildContext context) => SplashScreen(),
  '/LoginScreen': (BuildContext context) => LoginScreen(),
  '/LogRegScreen': (BuildContext context) => LogReg(),
  '/RegistrationScreen': (BuildContext context) => RegistrationScreen(),
  '/BottomBarElement': (BuildContext context) => BottomBarElement(),
  '/courseScreen': (BuildContext context) => CourseScreen(),
  '/mainScreen': (BuildContext context) => MainScreen(),
};
