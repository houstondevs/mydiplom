import 'package:diplom/Screens/BottomBar.dart';
import 'package:diplom/Screens/LoginScreen.dart';
import 'package:flutter/material.dart';
import 'package:diplom/Screens/SplashScreen.dart';
import 'package:diplom/settings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Python++',
      theme: courseTheme,
      home: SplashScreen(),
      routes: courseRoutes,
    );
  }
}
