class LoginModel {
  final String phoneNumber;
  final String token;
  final int userId;

  LoginModel(this.phoneNumber, this.token, this.userId);

  LoginModel.fromJson(Map<String, dynamic> json)
      : token = json['access'],
        phoneNumber = json['phone_number'],
        userId = json['pk'];
}
