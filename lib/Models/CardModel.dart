class CardModel{
  int pk;
  String titleUrl;
  String title;
  String description;


  CardModel({this.pk, this.title, this.titleUrl, this.description});

  factory CardModel.fromJson(Map<String, dynamic> json){
    return CardModel(pk : json['pk'],
        titleUrl : json['title_image'],
        title : json['title'],
        description : json['description']);
  }   
}