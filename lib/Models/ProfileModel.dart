class ProfileModel {
  final String photoUrl;
  final String firstName;
  final String middleName;
  final String lastName;
  final String email;
  final String phoneNumber;

  ProfileModel(this.phoneNumber, this.firstName, this.middleName, this.lastName, this.email, this.photoUrl);

  ProfileModel.fromJson(Map<String, dynamic> json)
      : phoneNumber = json['phone_number'],
        firstName = json['first_name'],
        photoUrl = json['image'],
        middleName = json['middle_name'],
        lastName = json['last_name'],
        email = json['email'];
}
